#! /usr/bin/env python3


# fetch-consultation-reforme-assemblee -- Fetch JSON data
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2017 Emmanuel Raviart
# https://framagit.org/paula.forteza/fetch-consultation-reforme-assemblee
#
# fetch-consultation-reforme-assemblee is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# fetch-consultation-reforme-assemblee is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Fetch JSON data from "Consultation pour une nouvelle Assemblée nationale".

https://consultation.democratie-numerique.assemblee-nationale.fr/
"""


import argparse
import itertools
import json
import os
import shutil
import sys
import urllib.parse

import requests


site_url = 'https://consultation.democratie-numerique.assemblee-nationale.fr'


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'target_dir',
        help='path of target directory containing JSON data',
        )
    args = parser.parse_args()

    target_dir = args.target_dir
    if os.path.exists(target_dir):
        for filename in os.listdir(target_dir):
            if filename.startswith('.'):
                continue
            file_path = os.path.join(target_dir, filename)
            if os.path.isdir(file_path):
                shutil.rmtree(file_path)
            else:
                os.remove(file_path)
    else:
        os.mkdir(target_dir)

    topics = []
    for page_number in itertools.count(1):
        response = requests.get(
            urllib.parse.urljoin(site_url, '/api/v2/topics'),
            params=dict(
                forum='59d6345924663755883c892a',
                page=page_number,
                ),
            )
        page_topics = response.json()['results']['topics']
        if not page_topics:
            break
        topics.extend(page_topics)

    for topic_summary in topics:
        topic_dir = os.path.join(target_dir, topic_summary['id'])
        if not os.path.exists(topic_dir):
            os.mkdir(topic_dir)
        response = requests.get(urllib.parse.urljoin(site_url, '/api/v2/topics/{}'.format(topic_summary['id'])))
        topic = response.json()['results']['topic']
        with open(os.path.join(topic_dir, 'topic.json'), 'w', encoding='utf-8') as json_file:
            json.dump(topic, json_file, ensure_ascii=False, indent=2, sort_keys=True)

        comments = []
        for page_number in itertools.count(1):
            response = requests.get(
                urllib.parse.urljoin(site_url, '/api/v2/comments'),
                params=dict(
                    page=page_number,
                    topicId=topic['id'],
                    ),
                )
            page_comments = response.json()['results']['comments']
            if not page_comments:
                break
            comments.extend(page_comments)
        comments_dir = os.path.join(topic_dir, 'comments')
        if not os.path.exists(comments_dir):
            os.mkdir(comments_dir)
        for comment in comments:
            with open(os.path.join(comments_dir, '{}.json'.format(comment['id'])), 'w', encoding='utf-8') as json_file:
                json.dump(comment, json_file, ensure_ascii=False, indent=2, sort_keys=True)

        with open(os.path.join(topic_dir, 'README.md'), 'w', encoding='utf-8') as markdown_file:
            markdown_file.write("## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?\n")
            markdown_file.write('\n')
            markdown_file.write("### Consultation pour une nouvelle Assemblée nationale\n")
            markdown_file.write('\n')
            markdown_file.write("# {}\n".format(topic['mediaTitle']))
            markdown_file.write('\n')
            for clause in topic['clauses']:
                markdown_file.write('{}\n'.format(clause['markup']))
            if comments:
                markdown_file.write('\n')
                markdown_file.write("#### Commentaires\n")
                for comment_index, comment in enumerate(comments):
                    markdown_file.write('\n')
                    if comment_index > 0:
                        markdown_file.write("----\n")
                        markdown_file.write('\n')
                    markdown_file.write('{}\n'.format(comment['text']))
                    markdown_file.write('\n')
                    markdown_file.write('Score : {} / Réponses : {}\n'.format(comment['score'], comment['repliesCount']))

    with open(os.path.join(target_dir, 'README.md'), 'w', encoding='utf-8') as markdown_file:
        markdown_file.write("# Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?\n")
        markdown_file.write('\n')
        markdown_file.write("## Consultation pour une nouvelle Assemblée nationale\n")
        markdown_file.write('\n')
        for topic in topics:
            markdown_file.write('* [{}](/{}/)\n'.format(topic['mediaTitle'], topic['id']))

    return 0


if __name__ == '__main__':
    sys.exit(main())
